<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Новости</li>
                            </ul>

                            <h1>Новости</h1>

                            <article class="news">
                                <div class="news__date">04.02.2018</div>
                                <div class="news__heading">
                                    <a href="#">Акция! Отбеливание зубов zoom4 со скидков 20%, всего за 49 990 руб.</a>
                                </div>
                                <div class="news__intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex</div>
                                <div class="text-right">
                                    <a href="#" class="views">подробнее</a>
                                </div>
                            </article>

                            <article class="news">
                                <div class="news__date">04.02.2018</div>
                                <div class="news__heading">
                                    <a href="#">Акция! Отбеливание зубов zoom4 со скидков 20%, всего за 49 990 руб.</a>
                                </div>
                                <div class="news__intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex</div>
                                <div class="text-right">
                                    <a href="#" class="views">подробнее</a>
                                </div>
                            </article>

                            <article class="news">
                                <div class="news__date">04.02.2018</div>
                                <div class="news__heading">
                                    <a href="#">Акция! Отбеливание зубов zoom4 со скидков 20%, всего за 49 990 руб.</a>
                                </div>
                                <div class="news__intro">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex</div>
                                <div class="text-right">
                                    <a href="#" class="views">подробнее</a>
                                </div>
                            </article>
                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
