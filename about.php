<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li>Стоматолог</li>
                            </ul>

                            <h1>Стоматолог</h1>

                            <p>Целью создания клиники «ТАЙМ» в Дубне было объединить коллектив высокопрофессиональных единомышленников, использующих научно-клинический подход, реализующих концепции профилактической, микроинвазивной и эстетической стоматологии, челюстно-лицевой хирургии, эстетической медицины в Дубне. Клиника обеспечивает доступность: </p>

                            <div class="content__row">
                                <div class="content__col">
                                    <ul class="list-point">
                                        <li>всех видов стоматологического лечения;</li>
                                        <li>протезирование;</li>
                                        <li>эстетические реставрации;</li>
                                        <li>имплантологические и костно-пластические операции;</li>
                                    </ul>
                                </div>
                                <div class="content__col">
                                    <ul class="list-point">
                                        <li>челюстно-лицевой области;</li>
                                        <li>коррекцией возрастных изменений;</li>
                                        <li>ортодонтические коррекция;</li>
                                        <li>лечением травм и заболеваний</li>
                                    </ul>
                                </div>

                                <div class="imageBox">
                                    <img src="images/personal.jpg" class="img-fluid" alt="">
                                </div>

                                <br/>

                                <h3>При организации нашей клиники основное внимание уделялось следующим направлениям:</h3>

                                <div class="direction">

                                    <div class="direction__item">
                                        <div class="direction__number">01</div>
                                        <div class="direction__text">Дизайну помещений -  это наша визитная карточка. Для обеспечения Вашего психологического и физического комфорта.</div>
                                    </div>

                                    <div class="direction__item">
                                        <div class="direction__number">02</div>
                                        <div class="direction__text">Основной принцип нашей работы - выполнение высокоточныхи качественных медицинских манипуляций.</div>
                                    </div>

                                    <div class="direction__item">
                                        <div class="direction__number">03</div>
                                        <div class="direction__text">Оборудованию и оснащению, а также новейшим технологиям, методикам и материалам.</div>
                                    </div>

                                    <div class="direction__item">
                                        <div class="direction__number">04</div>
                                        <div class="direction__text">Принципиальная позиция руководителя - доступный  уровень цен на оказанные услуги.</div>
                                    </div>

                                    <div class="direction__item long">
                                        <div class="direction__number">05</div>
                                        <div class="direction__text">Квалификации специалистов - наши специалисты имеют немалый опыт клинической работы, высокий уровень профессиональной подготовки, ученые степени, что позволяет решать любую клиническую задачу, какой бы сложной она не была.</div>
                                    </div>
                                </div>

                                <p>Клиника предназначена для людей с особенными требованиями к сервису, качеству и комфорту, людей, которые ценят свое здоровье, красоту и индивидуальность. Мы приверженцы персональной стоматологии и индивидуального подхода к лечению. Каждый наш пациент может принимать участие в выборе предложенных специалистом вариантов лечения. Мы не лечим всех одинаково, несмотря на соблюдение стандартов и протоколов лечения. После консультации пациенту представляется комплексный план лечения. Врач совместно с пациентом планирует последовательность лечебных процедур. Мы стремимся стать лучшими и имеем для этого все необходимое!</p>

                                <ul class="button-group">
                                    <li>
                                        <a href="#" class="btn btn-arrow btn-md btn-border">
                                            услуги
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#order" class="btn btn-md btn-modal">записаться на прием</a>
                                    </li>
                                </ul>

                            </div>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
