<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <header class="header header_home">
                <div class="container">
                    <div class="header__wrap">

                        <a href="#"  class="header__logo">
                            <img src="img/header__logo.png" class="img-fluid" alt="">
                        </a>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#order" class="header__nav_appointment btn-modal">Запись на прием</a></li>
                                <li>

                                    <i></i>
                                    <a href="#">О клинике</a>
                                    <ul class="dropdown">
                                        <li><a href="#">О нас</a></li>
                                        <li><a href="#">График работы</a></li>
                                        <li><a href="#">Полезная информация</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Специалисты</a></li>
                                <li>
                                    <i></i>
                                    <a href="#">Услуги</a>
                                    <ul class="dropdown">
                                        <li><a href="#">Притезирование</a></li>
                                        <li><a href="#">Хирургия</a></li>
                                        <li><a href="#">Имплантология</a></li>
                                        <li><a href="#">Эстетическая стоматология</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Цены</a></li>
                                <li><a href="#">Галерея</a></li>
                                <li><a href="#">Отзывы</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </nav>
                        <ul class="header__contact">
                            <li><a href="tel:+74958828282" class="tel">+7 (495) 882-82-82</a></li>
                            <li><a href="#callback" class="btn-callback btn-modal">Заказать звонок</a></li>
                        </ul>
                        <a href="#" class="header__toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
            </header>

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="jumbotron">
                            <div class="jumbotron__contact">

                                <div class="formBox">
                                    <div class="formBox__title">Записаться на осмотр</div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="" placeholder="Имя">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="" placeholder="Телефон">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md">записаться</button>
                                    </div>
                                    <div class="formBox__text">Нажимая на кнопку вы соглашатесь  на обработку персональных данных</div>
                                </div>

                                <ul class="jumbotron__info">
                                    <li>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 22 28" xmlns="http://www.w3.org/2000/svg">
                                                <g transform="translate(-92 -582)">
                                                    <use xlink:href="img/sprite-icons.svg#icon-location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </g>
                                            </svg>
                                        </i>
                                        <span>г. Дубна, ул. Вокзальная, 7</span>
                                    </li>
                                    <li>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg">
                                                <g transform="translate(-91 -612)">
                                                    <use xlink:href="img/sprite-icons.svg#icon-worktime" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </g>
                                            </svg>
                                        </i>
                                        <span>Работаем с 9:00 до 20:00</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="jumbotron__slider">
                                <div class="baseSlider">
                                    <div class="baseSlider__item">
                                        <a href="#">
                                            <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="baseSlider__item">
                                        <a href="#">
                                            <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="baseSlider__item">
                                        <a href="#">
                                            <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="baseSlider__item">
                                        <a href="#">
                                            <img src="img/slide_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="advantage">
                            <div class="advantage__item">
                                <div class="advantage__image">
                                    <svg class="ico-svg" viewBox="0 0 66 69" xmlns="http://www.w3.org/2000/svg">
                                        <g transform="translate(-92 -712)">
                                            <use xlink:href="img/sprite-icons.svg#icon-hardware" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </g>
                                    </svg>
                                </div>
                                <div class="advantage__heading">Новейшее оборудование</div>
                                <div class="advantage__text">У нас в клинике одно из лучших  оборудований из Германии</div>
                            </div>
                            <div class="advantage__item">
                                <div class="advantage__image">
                                    <svg class="ico-svg" viewBox="0 0 61 75" xmlns="http://www.w3.org/2000/svg">
                                        <g transform="translate(-491 -708)">
                                            <use xlink:href="img/sprite-icons.svg#icon-user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </g>
                                    </svg>
                                </div>
                                <div class="advantage__heading">Команда профессионалов</div>
                                <div class="advantage__text">Все врачи имеют высшее образование  и большой опыт работы в клинике</div>
                            </div>
                            <div class="advantage__item">
                                <div class="advantage__image">
                                    <svg class="ico-svg" viewBox="0 0 66 69" xmlns="http://www.w3.org/2000/svg">
                                        <g transform="translate(-92 -712)">
                                            <use xlink:href="img/sprite-icons.svg#icon-hardware" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </g>
                                    </svg>
                                </div>
                                <div class="advantage__heading">Новейшее оборудование</div>
                                <div class="advantage__text">У нас в клинике одно из лучших  оборудований из Германии</div>
                            </div>
                        </div>

                        <div class="preview">
                            <div class="preview__article">
                                <div class="heading"><strong>Статьи</strong> <a href="#">все статьи</a></div>
                                <div class="articleItem">
                                    <div class="articleItem__title"><a href="#">Как отбелить зубы</a></div>
                                    <div class="articleItem__text">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                    <div class="articleItem__link">
                                        <a href="#">подробнее</a>
                                    </div>
                                </div>
                                <div class="articleItem">
                                    <div class="articleItem__title"><a href="#">Как отбелить зубы</a></div>
                                    <div class="articleItem__text">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                    <div class="articleItem__link">
                                        <a href="#">подробнее</a>
                                    </div>
                                </div>


                            </div>
                            <div class="preview__news">
                                <div class="preview__news_image">
                                    <img src="img/news__image.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="preview__news_content">
                                    <div class="heading"><strong>Новости</strong> <a href="#">все новости</a></div>
                                    <div class="newsItem">
                                        <div class="newsItem__title"><a href="#">Мы запустили сайт! Ура, товарищи!</a></div>
                                        <div class="newsItem__date">04.02.2018</div>
                                    </div>
                                    <div class="newsItem">
                                        <div class="newsItem__title"><a href="#">Акция! Отбеливание зубов zoom4 со скидков 20%, всего за 49 990 руб.</a></div>
                                        <div class="newsItem__date">04.02.2018</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="services">
                            <h2>Услуги</h2>
                            <ul class="services__list">
                                <li>
                                    <a href="#">
                                        <img src="img/service__image_01.png" class="img-fluid" alt="">
                                        <div class="services__name"><span>стоматология</span><i class="fas fa-chevron-right"></i></div>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <img src="img/service___image_02.jpg" class="img-fluid" alt="">
                                        <div class="services__name"><span>челюстно-лицевая хирургия</span> <i class="fas fa-chevron-right"></i></div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="user">

                            <div class="heading"><strong>Специалисты</strong> <a href="#">все специалисты</a></div>

                            <div class="user__slider">

                                <div class="user__item">
                                    <div class="user__box">
                                        <div class="user__image">
                                            <img src="images/user_01.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="user__name">Вячеслав Ларионов </div>
                                        <div class="user__skills">
                                            Стоматолог<br/>
                                            Опыт работы 5 лет
                                        </div>
                                        <div class="user__review">
                                            <a href="#">Отзывы о враче (3)</a>
                                        </div>
                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                    </div>
                                </div>

                                <div class="user__item">
                                    <div class="user__box">
                                        <div class="user__image">
                                            <img src="images/user_02.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="user__name">Василий Баринов </div>
                                        <div class="user__skills">
                                            Стоматолог<br/>
                                            Опыт работы 5 лет
                                        </div>
                                        <div class="user__review">
                                            <a href="#">Отзывы о враче (3)</a>
                                        </div>
                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                    </div>
                                </div>

                                <div class="user__item">
                                    <div class="user__box">
                                        <div class="user__image">
                                            <img src="images/user_03.png" class="img-fluid" alt="">
                                        </div>
                                        <div class="user__name">Светлана Афанасьева</div>
                                        <div class="user__skills">
                                            Стоматолог<br/>
                                            Опыт работы 5 лет
                                        </div>
                                        <div class="user__review">
                                            <a href="#">Отзывы о враче (3)</a>
                                        </div>
                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                    </div>
                                </div>

                                <div class="user__item">
                                    <div class="user__box">
                                        <div class="user__image">
                                            <img src="images/user_02.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="user__name">Василий Баринов </div>
                                        <div class="user__skills">
                                            Стоматолог<br/>
                                            Опыт работы 5 лет
                                        </div>
                                        <div class="user__review">
                                            <a href="#">Отзывы о враче (3)</a>
                                        </div>
                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="miniGallery">
                            <div class="heading"><strong>Фотогалерея</strong> <a href="#">все фотографии</a></div>
                            <div class="miniGallery__slider">
                                <div class="miniGallery__item">
                                    <a href="images/gallery_01@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_01@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="miniGallery__item">
                                    <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="miniGallery__item">
                                    <a href="images/gallery_03@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_03@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="miniGallery__item">
                                    <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="contacts">
                    <div class="contacts__map">
                        <div id="map"></div>
                        <div class="contactBox">
                            <a href="#" class="contactBox__close"></a>

                            <ul class="contactBox__info">
                                <li>
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 22 28" xmlns="http://www.w3.org/2000/svg">
                                            <g transform="translate(-92 -582)">
                                                <use xlink:href="img/sprite-icons.svg#icon-location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </g>
                                        </svg>
                                    </i>
                                    <span>г. Дубна, ул. Вокзальная, 7</span>
                                </li>
                                <li>
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg">
                                            <g transform="translate(-91 -612)">
                                                <use xlink:href="img/sprite-icons.svg#icon-worktime" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </g>
                                        </svg>
                                    </i>
                                    <span>Работаем с 9:00 до 20:00</span>
                                </li>
                            </ul>

                            <ul class="contactBox__contact">
                                <li><a href="tel:+74958828282" class="tel">+7 (495) 882-82-82</a></li>
                                <li><a href="#callback" class="btn-callback btn-modal">Заказать звонок</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="contacts__form">
                        <div class="formBox">
                            <div class="formBox__title">Запишитесь на прием, заполнив форму</div>
                            <div class="formBox__subtitle">Наш специалист позвонит в ближайшее время после отправки</div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="" placeholder="Имя">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="" placeholder="Телефон">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-md">записаться</button>
                            </div>
                            <div class="formBox__text">Нажимая на кнопку вы соглашатесь  на обработку персональных данных</div>
                        </div>
                    </div>
                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->


        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <!--- Contact -->
        <script>

            ymaps.ready(init);

            function init() {
                var myMap = new ymaps.Map("map", {
                    center: [56.72714606799689,37.143091499999954],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                });

                var myPlacemark = new ymaps.Placemark([56.72714606799689,37.143091499999954], {
                    balloonContent: '',
                    iconCaption: ''
                }, {
                    preset: 'islands#greenDotIconWithCaption',
                    iconColor: '#0095b6'
                });

                myMap.geoObjects.add(myPlacemark);

                myPlacemark.events.add('click', function (e) {
                    $('.contactBox').show();
                });
            }
        </script>
        <!--- -->

    </body>
</html>
