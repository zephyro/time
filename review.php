<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Статьи</li>
                            </ul>

                            <div class="reviewHeading">
                                <h1>Статьи</h1>
                                <div class="reviewHeading__filter">
                                    <div class="reviewHeading__filter_label">Показать:</div>
                                    <div class="reviewHeading__filter_select">
                                        <select class="form-select">
                                            <option value="всех врачей">всех врачей</option>
                                            <option value="Стоматологов">Стоматологов</option>
                                            <option value="Хирургов">Хирургов</option>
                                            <option value="Ортодонтов">Ортодонтов</option>
                                        </select>
                                    </div>
                                    <a href="#review" class="reviewHeading__new btn-modal">
                                        <svg class="ico-svg" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite-icons.svg#icon-review" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                            <div class="review__row">

                                <div class="review">
                                    <div class="review__meta">
                                        <ul>
                                            <li>Дата:</li>
                                            <li>10.01.2018</li>
                                        </ul>
                                        <ul>
                                            <li>О враче:</li>
                                            <li>
                                                <div class="review__doctor">
                                                    <span>Маргарита Леонтьева</span>
                                                    <div class="review__tooltip">
                                                        <div class="review__tooltip_photo">
                                                            <img src="images/doctor.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="review__tooltip_name">Маргарита Леонтьева</div>
                                                        <div class="review__tooltip_skills">
                                                            Стоматолог<br/>
                                                            Опыт работы 5 лет
                                                        </div>
                                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Оценка:</li>
                                            <li><div class="rate-content"  data-score="3" data-readOnly="true"></div></li>
                                        </ul>
                                    </div>
                                    <div class="review__text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae </p>
                                    </div>
                                    <div class="review__author">Екатерина Смирнова, 26 лет</div>
                                </div>

                                <div class="review">
                                    <div class="review__meta">
                                        <ul>
                                            <li>Дата:</li>
                                            <li>10.01.2018</li>
                                        </ul>
                                        <ul>
                                            <li>О враче:</li>
                                            <li>
                                                <div class="review__doctor">
                                                    <span>Маргарита Леонтьева</span>
                                                    <div class="review__tooltip">
                                                        <div class="review__tooltip_photo">
                                                            <img src="images/doctor.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="review__tooltip_name">Маргарита Леонтьева</div>
                                                        <div class="review__tooltip_skills">
                                                            Стоматолог<br/>
                                                            Опыт работы 5 лет
                                                        </div>
                                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Оценка:</li>
                                            <li><div class="rate-content"  data-score="5" data-readOnly="true"></div></li>
                                        </ul>
                                    </div>
                                    <div class="review__text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae </p>
                                    </div>
                                    <div class="review__author">Екатерина Смирнова, 26 лет</div>
                                </div>

                                <div class="review">
                                    <div class="review__meta">
                                        <ul>
                                            <li>Дата:</li>
                                            <li>10.01.2018</li>
                                        </ul>
                                        <ul>
                                            <li>О враче:</li>
                                            <li>
                                                <div class="review__doctor">
                                                    <span>Маргарита Леонтьева</span>
                                                    <div class="review__tooltip">
                                                        <div class="review__tooltip_photo">
                                                            <img src="images/doctor.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="review__tooltip_name">Маргарита Леонтьева</div>
                                                        <div class="review__tooltip_skills">
                                                            Стоматолог<br/>
                                                            Опыт работы 5 лет
                                                        </div>
                                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Оценка:</li>
                                            <li><div class="rate-content"  data-score="4" data-readOnly="true"></div></li>
                                        </ul>
                                    </div>
                                    <div class="review__text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae </p>
                                    </div>
                                    <div class="review__author">Екатерина Смирнова, 26 лет</div>
                                </div>

                                <div class="review">
                                    <div class="review__meta">
                                        <ul>
                                            <li>Дата:</li>
                                            <li>10.01.2018</li>
                                        </ul>
                                        <ul>
                                            <li>О враче:</li>
                                            <li>
                                                <div class="review__doctor">
                                                    <span>Маргарита Леонтьева</span>
                                                    <div class="review__tooltip">
                                                        <div class="review__tooltip_photo">
                                                            <img src="images/doctor.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="review__tooltip_name">Маргарита Леонтьева</div>
                                                        <div class="review__tooltip_skills">
                                                            Стоматолог<br/>
                                                            Опыт работы 5 лет
                                                        </div>
                                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Оценка:</li>
                                            <li><div class="rate-content"  data-score="5" data-readOnly="true"></div></li>
                                        </ul>
                                    </div>
                                    <div class="review__text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae </p>
                                    </div>
                                    <div class="review__author">Екатерина Смирнова, 26 лет</div>
                                </div>

                                <div class="review">
                                    <div class="review__meta">
                                        <ul>
                                            <li>Дата:</li>
                                            <li>10.01.2018</li>
                                        </ul>
                                        <ul>
                                            <li>О враче:</li>
                                            <li>
                                                <div class="review__doctor">
                                                    <span>Маргарита Леонтьева</span>
                                                    <div class="review__tooltip">
                                                        <div class="review__tooltip_photo">
                                                            <img src="images/doctor.jpg" class="img-fluid" alt="">
                                                        </div>
                                                        <div class="review__tooltip_name">Маргарита Леонтьева</div>
                                                        <div class="review__tooltip_skills">
                                                            Стоматолог<br/>
                                                            Опыт работы 5 лет
                                                        </div>
                                                        <a href="#order" class="btn btn-border btn-modal">записаться</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>Оценка:</li>
                                            <li><div class="rate-content"  data-score="4" data-readOnly="true"></div></li>
                                        </ul>
                                    </div>
                                    <div class="review__text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae </p>
                                    </div>
                                    <div class="review__author">Екатерина Смирнова, 26 лет</div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
