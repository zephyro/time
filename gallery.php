<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li>Галерея</li>
                        </ul>

                        <h1>Галерея</h1>

                        <div class="gallery">
                            <div class="gallery__item">
                                <a href="images/gallery_01@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_01@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_03@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_03@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_03@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_03@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_01@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_01@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_01@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_01@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="gallery__item">
                                <a href="images/gallery_03@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                    <img src="images/gallery_03@2x.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
