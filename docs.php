<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Документы</li>
                            </ul>

                            <h1>Документы</h1>

                            <ul class="docs">
                                <li><a href="#"><span>Договор на оказание медицинских услуг</span></a></li>
                                <li><a href="#"><span>Договор о дентальной имплантации</span></a></li>
                                <li><a href="#"><span>Информированное добровольное согласие пациента на ортодонтическое лечение</span></a></li>
                                <li><a href="#"><span>Информативное добровольное согласие пациента на дентальную имплантацию</span></a></li>
                                <li><a href="#"><span>Памятка пациенту после имплантации</span></a></li>
                                <li><a href="#"><span>Рекомендации после отбеливания зубов</span></a></li>
                            </ul>
                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
