<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Специалисты</a></li>
                                <li>Быченков Артем Сергеевич</li>
                            </ul>

                            <div class="doctor">
                                <div class="doctor__heading">
                                    <h1>Быченков Артем Сергеевич</h1>
                                    <i>Челюстно–лицевой хирург, имплантолог</i>
                                </div>
                                <div class="doctor__photo">
                                    <img src="images/doctor.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="doctor__skills">
                                    <h3>Специалист в области:</h3>
                                    <ul class="list-base">
                                        <li>Классическая имплантация;</li>
                                        <li>Имплантация с моментальной нагрузкой;</li>
                                        <li>Классическая имплантация;</li>
                                        <li>Имплантация с моментальной нагрузкой;</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="doctor__box">
                                <h3>Образование:</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et </p>
                                <p>fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                                <p>beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut </p>
                            </div>

                            <div class="doctor__box">
                                <h3>Курсы и семинары:</h3>
                                <ul class="list-base">
                                    <li>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                        </p>
                                    </li>
                                    <li>
                                        <p>fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                        </p>
                                    </li>
                                    <li>
                                        <p>beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                    </li>
                                </ul>
                            </div>

                            <ul class="button-group">
                                <li>
                                    <a href="#" class="btn btn-arrow btn-border">
                                        Отзывы о враче
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#order" class="btn btn-md btn-modal">записаться на прием</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
