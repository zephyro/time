<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">



                        </div>

                    </div>
                </div>

                <div class="post">
                    <div class="post__top">
                        <div class="post__wrap">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Статьи</a></li>
                                <li>Название статьи</li>
                            </ul>
                            <h1>Как отбелить зубы</h1>
                        </div>
                        <div class="post__image">
                            <div class="post__image_wrap">
                                <img src="images/article__image.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="post__wrap">
                            <div class="post__intro">
                                Желание отбелить зубы может доставить
                                много хлопот. Услуги профессионалов
                                стоят очень дорого, а такая процедура
                                может помочь...
                            </div>
                        </div>
                    </div>
                    <div class="post__wrap">
                        <div class="post__content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                            <p>doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam </p>
                        </div>
                        <div class="share">
                            <div class="share__title">Поделиться новостью:</div>
                            <ul class="share__items">
                                <li><a href="#"><i class="fab fa-vk"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
