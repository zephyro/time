<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li>Специалисты</li>
                        </ul>

                        <h1>Специалисты</h1>

                        <div class="specialist">

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Вячеслав Ларионов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Василий Баринов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Светлана Афанасьева</div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Вячеслав Ларионов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Василий Баринов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Светлана Афанасьева</div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Вячеслав Ларионов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                            <div class="specialist__item">
                                <div class="specialist__image">
                                    <img src="images/user_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="specialist__name">Василий Баринов </div>
                                <div class="specialist__skills">
                                    Стоматолог<br/>
                                    Опыт работы 5 лет
                                </div>
                                <div class="specialist__review">
                                    <a href="#">Отзывы о враче (3)</a>
                                </div>
                                <a href="#order" class="btn btn-border btn-modal">записаться</a>
                            </div>

                        </div>

                    </div>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
