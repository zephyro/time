<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">О компании</a></li>
                                <li>Оборудование</li>
                            </ul>

                            <h1>Оборудование</h1>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                            <p>doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam </p>

                            <br/>

                            <div class="photoGallery">
                                <div class="photoGallery__item">
                                    <a href="images/gallery_01@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_01@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="photoGallery__item">
                                    <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="photoGallery__item">
                                    <a href="images/gallery_03@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_03@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="photoGallery__item">
                                    <a href="images/gallery_02@2x.jpg" class="img-fluid" data-fancybox="gallery">
                                        <img src="images/gallery_02@2x.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
