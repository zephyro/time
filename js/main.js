$('.header__toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.header').toggleClass('openNav');
});

$('.header__nav i').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});

$(".dropnav")
    .mouseover(function() {
        $('.header').addClass('drop');
    })
    .mouseout(function(){
        $('.header').removeClass('drop');
    });


$(".btn-modal").fancybox({
    btnTpl : {
        smallBtn : '<button data-fancybox-close class="modal__close" title="{{CLOSE}}"></button>'
    }
});

// Placeholders

$('.input-wrap input').focus(function(event) {
    $(this).closest('.input-wrap').addClass('focus');
});

$('.input-wrap input').focusout(function(){

    var inputVal = $(this).closest('.input-wrap').find('input').val();
    if (inputVal == '') {
        $(this).closest('.input-wrap').removeClass('focus');
    }
});

$('.contactBox__close').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.contactBox').hide();
});

$('.price__position').on('click touchstart', function(e) {
    e.preventDefault();

    var box = $(this).closest('.price__elem');

    if (box.hasClass('open')) {

        box.removeClass('open');

    }
    else {
        $(this).closest('.price').find('.price__elem').removeClass('open');
        box.addClass('open');
    }
});


$(function(){
    $(document).click(function(event) {
        if ($(event.target).closest(".price__elem").length) return;
        $(document).find('.price__elem').removeClass('open');
        event.stopPropagation();
    });
});

$('.price__heading').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
    $(this).closest('li').find('.price__content').slideToggle('fast');
});



$('.baseSlider').slick({
    dots: true,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false,
            }
        }
    ]
});

$('.user__slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }
    ]
});

$('.miniGallery__slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }
    ]
});

$('.photoGallery').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});

jQuery(document).ready(function(){

    $("input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $(this).closest('.file-label').find('span').text(filename);
    });
});


$('.rate-content').raty({
    number: 5,
    path    : 'img',
    score: function() {
        return $(this).attr('data-number');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    }
});
