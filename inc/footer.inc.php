<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="wrap">
                <ul class="footer__nav">
                    <li><a href="#">О клинике</a></li>
                    <li><a href="#">Специалисты</a></li>
                    <li><a href="#">услуги</a></li>
                    <li><a href="#">цены</a></li>
                    <li><a href="#">галерея</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">контакты</a></li>
                    <li><a href="#">статьи</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">вакансии</a></li>
                    <li>
                        <a href="#" class="footer__doc">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 20 23" xmlns="http://www.w3.org/2000/svg">
                                    <g transform="translate(-1164 -3110)">
                                        <use xlink:href="img/sprite-icons.svg#icon-doc" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </g>
                                </svg>
                            </i>
                            <span>документы</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="wrap">
                <ul class="footer__contact">
                    <li><a href="tel:+74958828282" class="tel">+7 (495) 882-82-82</a></li>
                    <li><a href="#callback" class="btn-callback">Заказать звонок</a></li>
                </ul>
                <div class="footer__copy">Клиника стоматологии и челюстно-лицевой  хирургии "Тайм" © 2018</div>
                <a href="#" class="footer__dev">Дизайн и разработка сайта</a>
            </div>
        </div>
    </div>
</footer>