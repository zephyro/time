<header class="header">
    <div class="container">
        <div class="header__wrap">

            <a href="#"  class="header__logo">
                <img src="img/header__logo.png" class="img-fluid" alt="">
            </a>
            <nav class="header__nav">
                <ul>
                    <li><a href="#order" class="header__nav_appointment btn-modal">Запись на прием</a></li>
                    <li class="dropnav">

                        <i></i>
                        <a href="#">О клинике</a>
                        <ul class="dropdown">
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">График работы</a></li>
                            <li><a href="#">Полезная информация</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Специалисты</a></li>
                    <li class="dropnav">
                        <i></i>
                        <a href="#">Услуги</a>
                        <ul class="dropdown">
                            <li>
                                <a href="#">Стоматология</a>
                                <ul>
                                    <li><a href="#">Протезирование</a></li>
                                    <li><a href="#">Гигиена</a></li>
                                    <li><a href="#">Терапия</a></li>
                                    <li><a href="#">Пародонтология</a></li>
                                    <li><a href="#">Ортодонтия</a></li>
                                    <li><a href="#">Отбеливание</a></li>
                                    <li><a href="#">Имплантация</a></li>
                                    <li><a href="#">Хирургия</a></li>
                                </ul>
                            </li>
                            <li><a href="#">челюстно-лицевая хирургия</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Цены</a></li>
                    <li><a href="#">Галерея</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <ul class="header__contact">
                <li><a href="tel:+74958828282" class="tel">+7 (495) 882-82-82</a></li>
                <li><a href="#callback" class="btn-callback btn-modal">Заказать звонок</a></li>
            </ul>
            <a href="#" class="header__toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
    </div>
</header>