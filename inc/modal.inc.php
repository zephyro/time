<!-- Заказать звонок -->
<div class="hide">
    <div class="modal" id="callback">
        <div class="modal__title">Заказать звонок</div>
        <div class="modal__text">Наш специалист позвонит в ближайшее время после отправки телефона</div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="phone" placeholder="Телефон">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-md btn-send">
                    <span>отправить</span>
                    <i>
                        <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
            </div>
            <div class="modal__bottom">Нажимая на кнопку вы соглашатесь на обработку персональных данных</div>
        </form>
    </div>
</div>
<!-- -->

<!-- Запись на прием -->
<div class="hide">
    <div class="modal" id="order">
        <div class="modal__title">
            Запишитесь<br/>
            на прием,<br/>
            заполнив форму
        </div>
        <div class="modal__text">Наш специалист позвонит в ближайшее время после отправки данных</div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Имя">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="phone" placeholder="Телефон">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-md btn-send">
                    <span>отправить</span>
                    <i>
                        <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
            </div>
            <div class="modal__bottom">Нажимая на кнопку вы соглашатесь на обработку персональных данных</div>
        </form>
    </div>
</div>
<!-- -->

<!-- Оставить отзыв -->
<div class="hide">
    <div class="modal" id="review">
        <div class="modal__title">Оставить отзыв</div>
        <form class="form">
            <div class="form-group">
                <div class="input-wrap">
                    <input type="text" class="form-control" name="phone" placeholder="">
                    <span class="input-placeholder">Имя <i>*</i></span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-wrap">
                    <input type="text" class="form-control" name="phone" placeholder="">
                    <span class="input-placeholder">Телефон или Email <i>*</i></span>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="form-col">
                        <select class="form-select" name="age">
                            <option value="">Возраст</option>
                            <option value="">21-30</option>
                            <option value="">31-40</option>
                            <option value="">41-50</option>
                        </select>
                    </div>
                    <div class="form-col">
                        <div class="input-wrap">
                            <select class="form-select" name="number">
                                <option value="">Оценка</option>
                                <option value="">21-30</option>
                                <option value="">31-40</option>
                                <option value="">41-50</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-wrap">
                    <textarea class="form-control" name="message" rows="5"></textarea>
                    <span class="input-placeholder">Отзыв <i>*</i></span>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-md btn-send">
                    отправить
                    <i>
                        <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
            </div>
            <div class="modal__bottom">Нажимая на кнопку вы соглашатесь на обработку персональных данных</div>
        </form>
    </div>
</div>
<!-- -->
