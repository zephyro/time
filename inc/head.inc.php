<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/fontawesome-all.css">
    <link  href="js/vendor/raty/jquery.raty.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/main.css">
</head>