<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Вакансии</a></li>
                                <li>Стоматолог</li>
                            </ul>

                            <h1>Стоматолог</h1>

                            <div class="vacancy">
                                <table class="vacancy__table">
                                    <tr>
                                        <td>График работы:</td>
                                        <td>Сменный</td>
                                    </tr>
                                    <tr>
                                        <td>Опыт:</td>
                                        <td>3 года</td>
                                    </tr>
                                    <tr>
                                        <td>Образование:</td>
                                        <td>высшее</td>
                                    </tr>
                                </table>

                                <h4>обязанности</h4>
                                <ul class="list-disk">
                                    <li>оказание квалифицированной стоматологической помощи терапевтического и хирургического профиля;</li>
                                </ul>


                                <h4>Требования</h4>
                                <ul class="list-disk">
                                    <li>наличие диплома об окончании медицинского института по соответствующей специальности;</li>
                                    <li>наличие действующего сертификата специалиста;</li>
                                    <li>опыт работы на терапевтическом и хирургическом приеме от трёх лет;</li>
                                </ul>

                                <h4>условия</h4>
                                <ul class="list-disk">
                                    <li>Постоянная работа, сменный график;</li>
                                </ul>

                            </div>

                            <ul class="button-group">
                                <li>
                                    <label class="file-label">
                                        <input type="file" name="file">
                                        <span>Прикрепить файл</span>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-clip" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </label>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-arrow btn-border">
                                        Отправить
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
