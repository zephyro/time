<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Цены</li>
                            </ul>

                            <h1>Цены</h1>

                        </div>

                    </div>

                </div>

                <div class="price__wrap">

                    <ul class="price">

                        <li>
                            <div class="price__heading"><i></i><span>Услуга</span></div>
                            <div class="price__content">
                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                            </div>
                        </li>

                        <li>
                            <div class="price__heading"><i></i><span>Услуга</span></div>
                            <div class="price__content">
                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                            </div>
                        </li>

                        <li>
                            <div class="price__heading"><i></i><span>Услуга</span></div>
                            <div class="price__content">
                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                            </div>
                        </li>

                        <li>
                            <div class="price__heading"><i></i><span>Услуга</span></div>
                            <div class="price__content">
                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info top">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info top">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info top">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                                <ul>
                                    <li>
                                        <div class="price__elem">
                                            <span class="price__position">Название подуслуги</span>
                                            <div class="price__info top">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                <div class="text-center">
                                                    <a class="btn btn-md btn-border btn-modal" href="#order">записаться</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span class="price__value">1 200 руб.</span></li>
                                </ul>

                            </div>
                        </li>

                    </ul>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
