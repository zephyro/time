<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Услуги</a></li>
                                <li>Стоматология</li>
                            </ul>

                            <h1>Стоматология</h1>

                            <div class="goods">
                                <div class="goods__item">
                                    <h4><a href="#">Протезирование</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">ортодонтия</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">гигиена</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">отбеливание</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">Терапия</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">имплантация</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">пародонтология</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                                <div class="goods__item">
                                    <h4><a href="#">хирургия</a></h4>
                                    <div class="goods__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis</div>
                                    <div class="text-right">
                                        <a href="#" class="views">ПОДРОБНЕЕ</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
