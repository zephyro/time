<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Статьи</li>
                            </ul>

                            <h1>Статьи</h1>

                            <div class="article__row">

                                <article class="article">
                                    <div class="article__image">
                                        <a href="#">
                                            <img src="images/article__image.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="article__main">
                                        <h4><a href="#">Как отбелить зубы</a></h4>
                                        <div class="article__intro">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                        <div class="text-right">
                                            <a href="#" class="views">подробнее</a>
                                        </div>
                                    </div>
                                </article>

                                <article class="article">
                                    <div class="article__image">
                                        <a href="#">
                                            <img src="images/article__image.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="article__main">
                                        <h4><a href="#">Как отбелить зубы</a></h4>
                                        <div class="article__intro">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                        <div class="text-right">
                                            <a href="#" class="views">подробнее</a>
                                        </div>
                                    </div>
                                </article>

                                <article class="article half">
                                    <div class="article__main">
                                        <h4><a href="#">Как отбелить зубы</a></h4>
                                        <div class="article__intro">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь. </div>
                                        <div class="text-right">
                                            <a href="#" class="views">подробнее</a>
                                        </div>
                                    </div>
                                </article>

                                <article class="article half">
                                    <div class="article__main">
                                        <h4><a href="#">Как отбелить зубы</a></h4>
                                        <div class="article__intro">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                        <div class="text-right">
                                            <a href="#" class="views">подробнее</a>
                                        </div>
                                    </div>
                                </article>

                                <article class="article">
                                    <div class="article__main">
                                        <h4><a href="#">Как отбелить зубы</a></h4>
                                        <div class="article__intro">Желание отбелить зубы может доставить много хлопот. Услуги профессионалов стоят очень дорого, а такая процедура может помочь</div>
                                        <div class="text-right">
                                            <a href="#" class="views">подробнее</a>
                                        </div>
                                    </div>
                                </article>

                            </div>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
