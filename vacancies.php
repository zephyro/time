<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li>Вакансии</li>
                        </ul>

                        <h1>Вакансии</h1>

                        <ul class="vacancies">
                            <li>
                                <a href="#">
                                    <img src="images/vacancies_01.jpg" class="img-fluid" alt="">
                                    <div class="vacancies__name"><span>стоматолог</span><i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="images/vacancies_02.jpg" class="img-fluid" alt="">
                                    <div class="vacancies__name"><span>челюстно-лицевой хирург</span> <i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/vacancies_02.jpg" class="img-fluid" alt="">
                                    <div class="vacancies__name"><span>стоматолог</span><i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="images/vacancies_01.jpg" class="img-fluid" alt="">
                                    <div class="vacancies__name"><span>челюстно-лицевой хирург</span> <i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
