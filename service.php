<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li>Услуги</li>
                        </ul>

                        <h1>Услуги</h1>

                        <ul class="services__list">
                            <li>
                                <a href="#">
                                    <img src="img/service__image_01.png" class="img-fluid" alt="">
                                    <div class="services__name"><span>стоматология</span><i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="img/service___image_02.jpg" class="img-fluid" alt="">
                                    <div class="services__name"><span>челюстно-лицевая хирургия</span> <i class="fas fa-chevron-right"></i></div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
