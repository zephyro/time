<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Услуги</a></li>
                                <li><a href="#">Стоматология</a></li>
                                <li>Протезирование</li>
                            </ul>

                            <h1>Протезирование</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </p>
                            <br/>
                            <br/>

                            <h3>Запишитесь на прием, заполнив форму</h3>
                            <div class="appointment">
                                <form class="form">
                                    <div class="appointment__row">
                                        <div class="appointment__column">
                                            <div class="form-group">
                                                <div class="input-wrap">
                                                    <input type="text" class="form-control" name="name" placeholder="">
                                                    <span class="input-placeholder">Имя</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="appointment__column">
                                            <div class="form-group">
                                                <div class="input-wrap">
                                                    <input type="text" class="form-control" name="phone" placeholder="">
                                                    <span class="input-placeholder">Телефон</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="appointment__column">
                                            <button type="submit" class="btn btn-md">записаться</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
