<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->


    <body>

            <div class="page">

                <!-- Head -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <section class="main">

                    <div class="container">

                        <div class="wrap">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li>Контакты</li>
                            </ul>

                            <h1>Контакты</h1>

                        </div>
                    </div>

                    <div class="contacts">
                        <div class="contacts__map">
                            <div id="map"></div>
                            <div class="contactBox">
                                <a href="#" class="contactBox__close"></a>

                                <ul class="contactBox__info">
                                    <li>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 22 28" xmlns="http://www.w3.org/2000/svg">
                                                <g transform="translate(-92 -582)">
                                                    <use xlink:href="img/sprite-icons.svg#icon-location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </g>
                                            </svg>
                                        </i>
                                        <span>г. Дубна, ул. Вокзальная, 7</span>
                                    </li>
                                    <li>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg">
                                                <g transform="translate(-91 -612)">
                                                    <use xlink:href="img/sprite-icons.svg#icon-worktime" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </g>
                                            </svg>
                                        </i>
                                        <span>Работаем с 9:00 до 20:00</span>
                                    </li>
                                </ul>

                                <ul class="contactBox__contact">
                                    <li><a href="tel:+74958828282" class="tel">+7 (495) 882-82-82</a></li>
                                    <li><a href="#callback" class="btn-callback btn-modal">Заказать звонок</a></li>
                                </ul>

                            </div>
                        </div>
                        <div class="contacts__form">
                            <div class="formBox">
                                <div class="formBox__title">Запишитесь на прием, заполнив форму</div>
                                <div class="formBox__subtitle">Наш специалист позвонит в ближайшее время после отправки</div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="" placeholder="Имя">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="" placeholder="Телефон">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-md">записаться</button>
                                </div>
                                <div class="formBox__text">Нажимая на кнопку вы соглашатесь  на обработку персональных данных</div>
                            </div>
                        </div>
                    </div>

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

            <!-- Modal -->
            <?php include('inc/modal.inc.php') ?>
            <!-- -->

            <!-- Scripts -->
            <?php include('inc/scripts.inc.php') ?>
            <!-- -->



            <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

            <!--- Contact -->
            <script>

                ymaps.ready(init);

                function init() {
                    var myMap = new ymaps.Map("map", {
                        center: [56.72714606799689,37.143091499999954],
                        zoom: 12
                    }, {
                        searchControlProvider: 'yandex#search'
                    });

                    var myPlacemark = new ymaps.Placemark([56.72714606799689,37.143091499999954], {
                        balloonContent: '',
                        iconCaption: ''
                    }, {
                        preset: 'islands#greenDotIconWithCaption',
                        iconColor: '#0095b6'
                    });

                    myMap.geoObjects.add(myPlacemark);

                    myPlacemark.events.add('click', function (e) {
                        $('.contactBox').show();
                    });
                }
            </script>
            <!--- -->

        </body>
</html>
