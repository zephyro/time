<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Head -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">

                <div class="container">

                    <div class="wrap">

                        <div class="content">

                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">О компании</a></li>
                                <li>Концепция</li>
                            </ul>

                            <h1>Концепция</h1>

                            <div class="tile">
                                <div class="tile__item">
                                    <h2>Философия клиники строится на трех базовых принципах:</h2>
                                </div>
                                <div class="tile__item">
                                    <div class="concept one">
                                        <div class="concept__wrap">профессионализм и высокая  квалификация сотрудников</div>
                                    </div>
                                </div>
                                <div class="tile__item">
                                    <div class="concept two">
                                        <div class="concept__wrap">ориентация удовлетворение потребностей каждого клиента</div>
                                    </div>
                                </div>
                                <div class="tile__item">
                                    <div class="concept three">
                                        <div class="concept__wrap">ответственность за результат и социальная значимость</div>
                                    </div>
                                </div>
                            </div>

                            <br/>

                            <h2>Как мы это видим</h2>

                            <div class="direction">

                                <div class="direction__item">
                                    <div class="direction__number">01</div>
                                    <div class="direction__text">
                                        <strong>Главная цель:</strong> помочь каждому, кто
                                        к нам обратился, выбрав для этого оптимальный и быстрый способ.
                                        Оказать помощь пациентам в открытии здорового и красивого образа жизни.
                                        Отличное состояние полости рта, удовлетворенность своим внешним видом- залог успешности.
                                    </div>
                                </div>

                                <div class="direction__item">
                                    <div class="direction__number">02</div>
                                    <div class="direction__text">
                                        <strong>Профессионализм и зание дела:</strong> специалисты клиники «ТАЙМ» - профессионалы своего дела. Наши
                                        врачи определят причину заболевания, подберут каждому пациенту оптимальный  план лечения, найдут индивидуальный подход к любому клиенту.


                                    </div>
                                </div>

                                <div class="direction__item">
                                    <div class="direction__number">03</div>
                                    <div class="direction__text">
                                        <strong>Ценности и преимущества:</strong> умение слушать – это позволяет специалистам устанавливать контакт с пациентом.
                                    </div>
                                </div>

                                <div class="direction__item">
                                    <div class="direction__number">04</div>
                                    <div class="direction__text">
                                        <strong>Исполнительность:</strong> порядок проведения
                                        и качества работ любого направления
                                        и уровня сложности курирует в нашей клинике главный врач.
                                    </div>
                                </div>

                                <div class="direction__item">
                                    <div class="direction__number">05</div>
                                    <div class="direction__text">
                                        <strong>Позитивность мышления:</strong> стимулирует действия и стремления к лучшим результатам в работе. Перед нами стоит задача оказать помощь всем, кто обратился в нашу клинику.
                                    </div>
                                </div>

                                <div class="direction__item">
                                    <div class="direction__number">06</div>
                                    <div class="direction__text">
                                        <strong>«Золотое» прикладное правило:</strong> мы верим в то, что к другим надо относится так, как хочешь, чтобы относились к тебе. Мы поступаем по- человечески в любой ситуации.
                                    </div>
                                </div>

                                <div class="direction__item long">
                                    <div class="direction__number">07</div>
                                    <div class="direction__text"><strong>Стремление к самосовершенствованию:</strong> специалисты нашей клиники постоянно повышают свою квалификацию, регулярно посещают семинары, как в России, так и за рубежом, проходя обучение на мастер-классах, участвуя в выставках и симпозиумах.</div>
                                </div>
                            </div>

                            <ul class="button-group">
                                <li>
                                    <a href="#" class="btn btn-arrow btn-md btn-border">
                                        услуги
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 19 12" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-arrow-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#order" class="btn btn-md btn-modal">записаться на прием</a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>


            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
